<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_income extends CI_Controller
    {
        public function index(){
            $data['site'] = "transaction_in";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('transaction/income/income');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $data['site'] = "transaction_in";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('transaction/income/input');
            $this->load->view('header-footer/footer');
        }

        public function edit(){
            $data['site'] = "transaction_in";
            $data['id'] = $this->input->get('getId');
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $data);
            $this->load->view('transaction/income/edit');
            $this->load->view('header-footer/footer');
        }

    }
    

?>