<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_installment extends CI_Controller
    {
        public function index(){
            $send['site'] = "piutang";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('installment/installment');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "piutang";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('installment/input');
            $this->load->view('header-footer/footer');
        }

    }
    

?>