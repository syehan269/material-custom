<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');

    class c_debt extends CI_Controller
    {
        public function index(){
            $send['site'] = "utang";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('debt/debt');
            $this->load->view('header-footer/footer');
            $this->load->view('function');
        }

        public function input(){
            $send['site'] = "utang";
            $this->load->view('header-footer/header');
            $this->load->view('sidebar-topbar/side', $send);
            $this->load->view('debt/input');
            $this->load->view('header-footer/footer');
        }

    }
    

?>