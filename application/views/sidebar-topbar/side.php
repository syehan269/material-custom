<div class="bungkus col-2">
    <nav id="side">
        <div class="sidebar-header d-inline-flex">
            <h4 class="my-auto c-text-primary">Material.</h4>
            <span class="logo my-auto"><p class="m-1 text-center medium-weight ">M</p></span>
        </div>

        <ul class="list-unstyled component">
            <?php if($site == "dashboard"): ?>
            <li class="d-inline-flex mt-3 subitem1">
                <i class="bx bxs-dashboard bx-md" style='color:#5756B3;'></i>
                <a href="<?php echo base_url() ?>index.php/Welcome" class="primary-title my-auto medium-weight">Dashboard</a>
            </li>
            <?php else: ?>            
            <li class="d-inline-flex mt-3 subitem1">
                <i class="bx bxs-dashboard bx-md"></i>
                <a href="<?php echo base_url() ?>index.php/Welcome" class="soft-title my-auto medium-weight">Dashboard</a>
            </li>
            <?php endif; ?>
            <?php if($site == "transaction_in"): ?>
            <li class=" mt-3" style="display:contents;">
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-dollar-circle  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Transaksi</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a class=" c-text-2 primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_income">Masuk</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_outcome">Keluar</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "transaction_out"): ?>
            <li class=" mt-3" style="display:contents;">
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-dollar-circle  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Transaksi</p>
                </a>
                <ul class="show list-unstyled" id="pageSubmenu">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class=" c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_income">Masuk</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a class="c-text-2  primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_outcome">Keluar</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>
            <li class=" mt-3" style="display:contents;">
                <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class= "dropdown-toggle medium-weight soft-title c-text-2" style="display: flex;">
                <i class='bx bxs-dollar-circle  my-auto bx-md ' style='color:#9999bd; margin-left:-10px;'></i><p class="medium-weight" style="display: initial; margin-bottom:0;">Transaksi</p>
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_income">Masuk</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-dollar-circle my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_outcome">Keluar</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if($site == "utang"): ?>
            <li class=" mt-1" style="display:contents;">
                <a href="#pagePerhutang" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-wallet-alt  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Perhutangan</p>
                </a>
                <ul class="show list-unstyled" id="pagePerhutang">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a class=" c-text-2 primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_debt">Utang</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_installment">Piutang</a>
                    </li>
                </ul>
            </li>
            <?php elseif($site == "piutang"): ?>
            <li class=" mt-1" style="display:contents;">
                <a href="#pagePerhutang" data-toggle="collapse" aria-expanded="true" class= "dropdown-toggle medium-weight c-text-2" style="display: flex;">
                <i class='bx bxs-wallet-alt  my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Perhutangan</p>
                </a>
                <ul class="show list-unstyled" id="pagePerhutang">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class=" c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_debt">Utang</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#5756B3; margin-left:20px;'  ></i>
                        <a class="c-text-2  primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_installment">Piutang</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>
            <li class=" mt-1" style="display:contents;">
                <a href="#pagePerhutang" data-toggle="collapse" aria-expanded="false" class= "dropdown-toggle medium-weight soft-title c-text-2" style="display: flex;">
                <i class='bx bxs-wallet-alt  my-auto bx-md ' style='color:#9999bd; margin-left:-10px;'></i><p class="medium-weight" style="display: initial; margin-bottom:0;">Perhutangan</p>
                </a>
                <ul class="collapse list-unstyled" id="pagePerhutang">
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_debt">Utang</a>
                    </li>
                    <li  class=" " style="display:flex;">
                        <i class='bx bxs-wallet-alt my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                        <a class="c-text-2 soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_installment">Piutang</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>

            <?php if($this->session->userdata("user_level") == "admin"): ?>
                <?php if($site == "customer"): ?>
                <li class="active mt-3" style="display:contents;">
                    <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                    </a>
                    <ul class="show list-unstyled" id="pageSubmenu2">
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                            <a class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                        </li>
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                        </li>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                        </li>   
                    </ul>
                </li>
                <?php elseif($site == "supplier"): ?>
                <li class="active mt-3" style="display:contents;">
                    <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                    </a>
                    <ul class="show list-unstyled" id="pageSubmenu2">
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                        </li>
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                            <a class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                        </li>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 soft-title' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                        </li>   
                    </ul>
                </li>
                <?php elseif($site == "material"): ?>
                <li class="active mt-3" style="display:contents;">
                    <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                    </a>
                    <ul class="show list-unstyled" id="pageSubmenu2">
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                        </li>
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                        </li>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2 primary-title' style=' margin-left:20px;'  ></i>
                            <a class="primary-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                        </li>  
                    </ul>
                </li>
                <?php elseif($site == "merk"): ?>
                <li class="active mt-3" style="display:contents;">
                    <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="true" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md ' style='color:#5756B3; margin-left:-10px;'></i><p class="medium-weight primary-title" style="display: initial; margin-bottom:0;">Data</p>
                    </a>
                    <ul class="show list-unstyled" id="pageSubmenu2">
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                        </li>
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                        </li>
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style=' margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                        </li>
                    </ul>
                </li>
                <?php else: ?>
                <li class="active mt-3" style="display:contents;">
                    <a href="#pageSubmenu2" data-toggle="collapse" aria-expanded="false" class= "soft-title dropdown-toggle medium-weight c-text-2" style="display: flex;">
                    <i class='bx bxs-pie-chart-alt-2 my-auto bx-md soft-title' style=' margin-left:-10px;'></i><p class="medium-weight soft-title" style="display: initial; margin-bottom:0;">Data</p>
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu2">
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_customer">Pelanggan</a>
                        </li>
                        <li class="active " style="display:flex;">
                        <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_supplier">Pemasok</a>
                        </li>
                        <li class="active" style="display:flex;">
                            <i class='bx bxs-pie-chart-alt-2 my-auto bx-md-2' style='color:#9999bd; margin-left:20px;'  ></i>
                            <a class="soft-title c-text-2" href="<?php echo base_url() ?>index.php/c_material">Material</a>
                        </li>   
                    </ul>
                </li>
                <?php endif; ?>
            <?php else: ?>
            <?php endif; ?>
        </ul>

    </nav>
</div>