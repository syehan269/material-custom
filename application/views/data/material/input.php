    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Tambah Material</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <form action="" method="post" class="col-12 p-0">
                <div class="flex-column col-12 main-padding-l pr-0">
                    <div class="col-12 p-0">
                        <p class="c-text-2 soft-title medium-weight" >ID Material</p>
                        <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="ID..." id="id">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Nama Material</p>
                        <input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..." id="material">
                    </div>
                    <div class="col-12 p-0 mt-4 d-none">
                        <p class="c-text-2 soft-title medium-weight">Brand Name</p>
                        <select name="name" id="brand" style="width: 100%" class="dropdown-select2 c-text-2 search-fill" >
                            <option value="">Select Brand</option>
                        </select>
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Beli</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Price..." id="price">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Distributor</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="priceDist">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Reguler</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="priceReg">
                    </div>
                    <div class="col-12 mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight" >Harga Eceran</p>
                        <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="priceEcer">
                    </div>
                    <button id="btnSubmit" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2" type="submit">Tambah Material</button>
                </div>
            </form>
        </div>
</div>

<script>
    $(document).ready(function () {
        $("#brand").select2();
        set_dropdown();

        //TODO: input brand masih kurang field di api
        $("#btnSubmit").click(function (e) { 
            e.preventDefault();
            var getMaterial = $("#material").val();
            var getID = $("#id").val();
            var getBrand = $("#brand").val();
            var getPrice = parseInt($("#price").val());
            var getInfo = $("#info").val();

            request = $.ajax({
                        url: 'http://153.92.4.88:8080/material',
                        type: 'post',
                        data: {
                            material_name: getMaterial,
                            material_merek_name: getBrand,
                            material_price: getPrice
                        }
            });

          request.done(function(response) {
              window.location.href = "<?php echo base_url() ?>index.php/c_material";
          });
          request.fail(function(response) {
              var success = response.success;
              var message = response.message;
              var data = response.data;
              console.log(success);
              console.log(message);
              console.log(data);
          });

        });
        
        function set_dropdown() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    for(i=0; i<obj.data.length; i++){
                        payload += '<option value="'+obj.data[i].merek_name+'">'+obj.data[i].merek_name+'</option>';
                        //$("#matName").append(payload);
                        $("#brand").html(payload);
                    }
                }
            });
        }
    });
</script>