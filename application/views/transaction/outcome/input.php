    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4" id="title">Tambah Transaksi Keluar</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <div class="col-6 main-padding-r">
                <button data-toggle="modal" data-target="#addNewMaterial" type="button" class="btn-outline col-12 c-border-primary primary-title c-main-background c-text-2 boldest-weight">
                    Tambah Material
                </button>
                <div class="mt-4 custom-card p-3" style="min-height: 400px !important;">
                    <table id="mainMaterialTable" class="col-12 p-3" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Brand</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Total</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="tbMaterial">
                            
                        </tbody>
                    </table>
                </div>
                <div>
                    <input type="hidden" id="customerId" palceholder="id customer">
                </div>
                <div>
                    <input type="hidden" id="cicilanCountVal" palceholder="jumlah cicilan" value="0">
                </div>
                <div>
                    <input type="hidden" id="materialCountVal" palceholder="jumlah material" value="0">
                </div>
                <div>
                    <input type="hidden" id="percentage" palceholder="persentase">
                </div>
                <div>
                    <input type="hidden" id="payment" palceholder="payment" value="0">
                </div>
                <div>
                    <input type="hidden" id="result" palceholder="result" value="0">
                </div>
                <div>
                    <table>
                        <tbody id="tbCicilan">

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="flex-column col-6 main-padding-l pr-0">
                <div class="col-12 p-0">
                    <p class="c-text-2 soft-title medium-weight">Nama Pembeli</p>
                    <!-- <input class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..."> -->
                    <select name="name" id="buyerName" style="width: 100%" class="dropdown-select2 c-text-2 search-fill" >
                        <option value="">Select Supplier</option>
                    </select>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Tipe Pembayaran</p>
                    <!-- <input class="col-11 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Payment..."> -->
                    <select class="c-dropdown col-11 c-text-2" id="paymentMethod">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Cash">Cash</option>
                        <option class="dropdown-item" value="Credit">Credit</option>
                    </select>
                    <a href="#">
                        <button id="btnCicilan" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                            <i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>
                        </button>
                    </a >
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Metode Pembelian</p>
                    <select id="sellInteract" class="c-dropdown col-12 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Takeaway">Take Away</option>
                        <option class="dropdown-item" value="Delivery">Delivery</option>
                    </select>
                </div>
                <div id="increasePrice">
                    
                </div>
                <div id="demo">
                    
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Jumlah Uang Pembelian</p>
                    <input id="" class="col-12 c-text-2 search-fill main-padding-l main-padding-r">
                </div>
                <div class="d-inline-flex col-12 p-0">
                    <div class="col-5 mt-4 p-0 mr-3">
                        <p class="c-text-2 soft-title medium-weight">Discount</p>
                        <input id="discountForm" class="col-12 c-text-2 secondary-field main-padding-l main-padding-r" disabled value="0% - Rp 0">
                    </div>
                    <div class="col-6 ml-auto mt-4 p-0">
                        <p class="c-text-2 soft-title medium-weight">Total Pembayaran</p>
                        <div class="p-0 d-inline-flex col-12">
                            <input id="countTotal" class="col-10 c-text-2 secondary-field main-padding-l main-padding-r" value="Rp 0" disabled>
                            <a href="#" class="my-auto">
                                <button id="btnRefresh" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                                    <i class="bx bx-refresh bx-xs text-white" style="margin-top: 5px"></i>
                                </button>
                            </a >
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Informasi Tambahan</p>
                    <textarea id="info-trans" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b" rows="4" style="min-height: 150px; width: 100%"></textarea>
                </div>
                <button id="addSubmit" class="btn-add col-12 c-text-2 text-white c-color-primary c-color-primary mt-4">Tambah Transaksi</button>
            </div>
        </div>
</div>
<!-- input cicilan  -->
<div class="modal fade" id="addCicilan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Cicilan</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <p class="c-text-2 soft-title regular-weight">Jumlah Cicilan</p>
            <input type="number" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" value="0" id="paymentCount">
        </div>
        <hr>
        <div class="col-12 p-0" id="countPay">

        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight" data-dismiss="modal">Batal</button>
        <button type="button" id="submitCicilan" class="btn-modal-positive medium-weight">Konfirmasi</button>
      </div>
    </div>
  </div>
</div>
<!-- input quantity -->
<div class="modal fade" id="modQuantMaterial" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-1">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Kuantitas Material</p>
            <input type="number" id="editQuant" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material brand</p>
            <input type="text" id="editBrand" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Harga Material</p>
            <input type="text" id="editPrice" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-3">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Harga Harian (Opsional)</p>
            <input type="number" id="editPriceDaily" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Harga Harian...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Nama Material</p>
            <input type="text" id="editName" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Kembali</button>
        <button id="addMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Tambah Material</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addNewMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" id="search-in" class="col-6 my-auto c-text-2 search-fill main-padding-l main-padding-r" placeholder="Cari Material...">
                <div class="ml-3 my-auto px-0 col-5">
                    <select class="c-dropdown col-12 c-text-2" id="filter-material-out" style="width: 70%;">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                    </select>
                </div>
                <div class="d-flex my-auto">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                    <table id="addNewTable" class="col-12 p-2" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">No.</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Brand</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="newMaterialBody">
                            
                        </tbody>
                    </table>
                </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </div>
  </div>
</div>

<!-- print out layout -->
<div class="modal fade" id="printOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Check Out</p>
      </div>
      <div class="modal-body c-main-background">
        
        <!-- INVOICE -->
        <div class="container-fluid py-2" style="border:solid black 2px">
            <div class="col h-100" style="position:relative">
                <div class="row">
                    <div class="w-25">
                        <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                    </div>
                </div>
                <div class="row w-100 h-100" style="position:absolute;top:0">
                    <h3 class="text-center font-weight-bold m-auto w-100"><b>INVOICE</b></h3>
                </div>
            </div>
            <div class="col mt-2">
                <tr><h4><b>TOKO BANGUNAN</b></h4></tr>
                <div class="row mt-2">
                    <div class="col-8">
                        <table>
                            <tr><h4><b>Alamat . . . .</b></h4></tr>
                            <tr><h4><b>Alamat . . . .</b></h4></tr>
                            <tr>
                                <td><b>Phone</b></td>
                                <td><b>:</b></td>
                                <td><b>(xxx - xxxxxxx)</b></td>
                            </tr>
                            <tr>
                                <td><b>Fax</b></td>
                                <td><b>:</b></td>
                                <td><b>(xxx - xxxxxxx)</b></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-4">
                        <table>
                            <tr>
                                <td><b>Invoice Date</b></td>
                                <td><b>:</b></td>
                                <td><b id="invoice-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Payment Term</b></td>
                                <td><b>:</b></td>
                                <td><b id="payment-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Package</b></td>
                                <td><b>:</b></td>
                                <td><b id="package-check"></b></td>
                            </tr>
                            <tr>
                                <td><b>Package Price</b></td>
                                <td><b>:</b></td>
                                <td><b id="package-price-check"></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col">
                <table>
                    <tr>
                        <td style="vertical-align:top"><b>Customer</b> </td>
                        <td style="vertical-align:top"><b>:</b> <span id="buyerName-check"></span></td>
                        <td><b><br><br>Phone :........................ Alamat : <span id="buyerAddress-check"></span> </b></td>
                    </tr>
                </table>
            </div>
            <div class="col mt-2">
                <table class="table font-weight-bold" >
                    <thead>
                        <tr>
                            <th><b>No.</b></th>
                            <th><b>Product Descriptions</b></th>
                            <th><b>Quantity</b></th>
                            <th><b>Unit Price</b></th>
                            <th><b>Amount</b></th>
                        </tr>
                    </thead>
                    <tbody id="check-out-table">

                    </tbody>
                </table>
                <div class="col text-right">
                    <b>
                        <h3>Amount Due : </h3>
                        <h2><b>Rp. <span id="check-out-total"></span></b></h2>
                    </b>
                </div>
            </div>
        </div>
        <!-- /.INVOICE -->
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Cancel</button>
        <button type="button" id="checkout-send" class="btn-modal-negative mr-3 medium-weight c-text-2 c-color-primary text-white">Submit</button>
        <!-- <button id="subMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button> -->
      </div>
    </div>
  </div>
</div>

<script>
    var getCicilanBundle = [];

    $(document).ready(function () {
        var table = "";
        $('.dropdown-select2').select2();
        $('#filter-material-out').select2();

        $("#search-in").on("input", function () {
            search(2, this.value);
        });

        $("#filter-material-out").on("change", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#addNewTable').DataTable()
                .search('').columns()
                .search('').draw();
            $("#filter-material-out").val("");
            $("#search-in").val("");
        });

        set_main_data();
        set_filter();
        set_dropdown_material();
        set_dropdown_brand();
        show_data();
        set_discount();

        checkout();

        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },
                columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    {
                        "targets": 2,
                        "visible": false
                    },
                    {
                        "targets": 3,
                        "visible": false
                    } 
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function set_main_data(){
            var e = document.getElementById("buyerName");
            var data1 = e.options[e.selectedIndex].value;
            
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/customer/"+data1,
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){
                        $.ajax({
                            type: "GET",
                            url: "http://153.92.4.88:8080/discount/"+obj.data[0].customer_discount_id,
                            async: true,
                            dataType: "text",
                            success: function (response) {
                                var j;
                                obj1 = JSON.parse(response);
                                
                                var percentage = obj1.data[0].discount_percentage;
                                var finPercentage = percentage+"% - Rp 0";
                                $("#discountForm").val(finPercentage);
                                console.log(percentage+"%");

                            }
                        });
                        $("#customerId").val(obj.data[0].customer_id);
                        $("#deliveryAddress").val(obj.data[0].customer_address);
                    }
                }
            });

        }

        function show_data(){

            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/material',
                async : true,
                dataType : 'text',
                success : function(response){
                    var html = '';
                    var i;
                    obj = JSON.parse(response);

                    for(i=0; i<obj.data.length; i++){
                        var setId = "new-mat-"+i;
                        var setMate = setId+"-mate";
                        var setBrand = setId+"-brand";
                        var setQuant = setId+"-quant";      
                        var setPrice = setId+"-price";
                        var earPrice = obj.data[i].material_price;
                        var finPrice = "Rp "+earPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

                        html += '<tr style="cursor: pointer;">'+
                                    '<td class="p-3 c-text-2 text-center">'+(i + 1)+'</td>'+
                                    '<td id='+setMate+' class="p-3 c-text-2 text-center">'+obj.data[i].material_name+'</td>'+
                                    '<td id='+setBrand+' class="p-3 c-text-2 text-center">'+obj.data[i].material_merek_name+'</td>'+
                                    '<td id='+setQuant+' class="p-3 c-text-2 text-center">0</td>'+
                                    '<td id='+setPrice+' class="p-3 c-text-2 text-center">'+finPrice+'</td>'+
                                    '<td class="text-center">'+
                                        '<a href="#" class="" onclick="showQuant(\''+setMate+'\',\''+setBrand+'\',\''+earPrice+'\')">'+
                                            '<button class="ml-2 basic-btn c-color-primary">'+
                                                '<i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>'+
                                            '</button>'+
                                        '</a>'+
                                    '</td>'+
                                '</tr>';    
        
                    }
                    $('#newMaterialBody').append(html);
                    dataTable();
                }
            });
        }

        function set_dropdown_brand(){
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value="'+obj.data[i].merek_name+'">'+obj.data[i].merek_name+'</option>';
                        $("#brandName").html(payload);
                        $("#editName").html(payload);
                    }
                }
            });
        }

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/customer",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].customer_id+'>'+obj.data[i].customer_name+'</option>';
                        $("#buyerName").html(payload);
                    }
                }
            });
        }

        function checkout(){
            var e = document.getElementById("buyerName");
            var data1 = e.options[e.selectedIndex].value;
            var f = document.getElementById("paymentMethod");
            var data2 = f.options[f.selectedIndex].value;
            var g = document.getElementById("sellInteract");
            var data3 = g.options[g.selectedIndex].value;

            var i;
            var totalKeseluruhan = 0;

            var totalGet = $("#materialCountVal").val();
            var totalDis = $("#percentage").val();
            var checkOutTable = '';

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;

            $("#invoice-check").html(today);
            $("#payment-check").html(data2);
            $("#package-check").html(data3);

            if(data3 == "Delivery"){
                var price = $("#deliveryPrice").val();
                $("#package-price-check").html("Rp. "+price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+",-");
            }else{
                $("#package-price-check").html("-");
            }

            for(i = 0; i < totalGet; i++){

                var realCount = i + 1;

                var mateName = $("#"+realCount+"-input-mate").val();
                var mateId = $("#"+realCount+"-input-id-mate").val();
                var brandName = $("#"+realCount+"-input-brand").val();
                var brandId = $("#"+realCount+"-input-id-brand").val();
                var getPrice = $("#"+(i+1)+"-price").text();
                var getMate = $("#"+(i+1)+"-name").text();
                
                var quantity = parseInt($("#temp-quant-"+realCount).val());

                var amount = getTotalPay();

                totalKeseluruhan = totalKeseluruhan + amount;

                checkOutTable += '<tr>'+
                                    '<th>'+realCount+'</th>'+
                                    '<th>'+getMate+'</th>'+
                                    '<th>'+quantity+'</th>'+
                                    '<th>Rp. '+getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+',-</th>'+
                                    '<th>Rp. '+amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+',-</th>'+
                                 '</tr>';
            }
            
            $("#check-out-table").html(checkOutTable);
            $("#check-out-total").html(totalKeseluruhan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));

            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/customer/"+data1,
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    obj = JSON.parse(response);
                    
                    $("#buyerName-check").html(obj.data[0].customer_name);
                    $("#buyerAddress-check").html(obj.data[0].customer_address);
                }
            });

        }

        function set_dropdown_material() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/material",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    for(i=0; i<obj.data.length; i++){
                        payload += '<option value="'+obj.data[i].material_name+'">'+obj.data[i].material_name+'</option>';
                        //$("#matName").append(payload);
                        $("#filter-material-out").html(payload);
                    }
                }
            });
        }

        function set_discount() {
            var getId = $("#buyerName option:selected").val();
            console.log(getId);
        }

        $("#addSubmit").click(function (e) { 
            e.preventDefault();

            if ($("#tbMaterial tr").length < 1) {
                alert("Input material first !");
            }else{
                $("#printOut").modal("show");
                totalQuant();
                checkout();
            }

        });

        $("#btnRefresh").click(function (e) { 
            e.preventDefault();
            totalQuant();
        });

        $("#checkout-send").click(function (e) { 
            var e = document.getElementById("buyerName");
            var data1 = $("#buyerName option:selected").text();
            var f = document.getElementById("buyerName");
            var data2 = f.options[f.selectedIndex].html;
            var g = document.getElementById("paymentMethod");
            var data3 = g.options[g.selectedIndex].value;
            var h = document.getElementById("sellInteract");
            var data4 = h.options[h.selectedIndex].value;
            var splitTotal = $("#countTotal").val();
            
            var totalPayment = getTotalPay();

            var info = $("#info").val();

            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();

            today = yyyy + '-' + mm + '-' + dd;

            var material_count = $("#materialCountVal").val();
            var cicilan_count = $("#cicilanCountVal").val();
            var i;
            var infoTrans = $("#info-trans").val();
            var detail_transaction = '';
            var delAdd = '';
            var delVal = 0;
            var getService = $("#sellInteract option:selected").val();
            var getDate = moment().format('YYYY/MM/DDThh:mm:ss');
            console.log(getDate);

            if (infoTrans.length == 0) {
                infoTrans = "null";                
            }

            if(data4 == "Delivery"){
                delAdd += $("#deliveryAddress").val();
                delVal += parseInt($("#deliveryPrice").val());
            }else{
                delAdd += "-";
                delVal += 0;
            }

            var totalAmount = parseInt($("#result").val());

            var endTotal = totalAmount + delVal;


            for(i = 0; i < material_count; i++){

                var realCount = i + 1;

                var mateName = $("#"+realCount+"-input-mate").val();
                var mateId = $("#"+realCount+"-input-id-mate").val();
                var brandName = $("#"+realCount+"-input-brand").val();
                var brandId = $("#"+realCount+"-input-id-brand").val();

                var quantity = parseInt($("#temp-quant-"+realCount).val());

                var price = 50000;

                var amount = quantity * 50000;

                detail_transaction += '{'+
                                          'detail_out_material_id: "'+mateId+'",'+
                                          'detail_out_material_name: "'+mateName+'",'+
                                          'detail_out_material_amount: "'+quantity+'",'+
                                          'detail_out_material_price: "'+price+'",'+
                                          'detail_out_merek_id: "'+brandId+'",'+
                                          'detail_out_merek_name: "'+brandName+'"'+
                                      '},';
            }

            console.log(detail_transaction);

            request = $.ajax({
                url: 'http://153.92.4.88:8080/transaction-out',
                type: 'post',
                dataType: 'JSON',
                data: {
                    customer_id: "null",
                    customer_name: data1,
                    payment_amount: totalPayment,
                    payment_type: data3,
                    delivery_type: getService,
                    delivery_address: delAdd,
                    due_date: getDate,
                    additional_info: infoTrans,
                    detail_trans: materialBundle(),
                    credit_details : cicilanBundle()
                }
            });

            request.done(function(response) {
                window.location.href = "<?php echo base_url('index.php/c_outcome') ?>";
            });

            request.fail(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;
            });
        });

        $("#buyerName").on("change", function () {
            set_main_data();
        });

        $("#payment").on("change", function () {
            var payment = $(this).find('option:selected').text()
            if (payment === "Cash") {
                $("#btnCicilan").prop("disabled", true);
            }else{
                $("#btnCicilan").prop("disabled", false);
            }
        });

        $("#sellInteract").on("change", function () {
            var action = $(this).find('option:selected').text()
            if (action === "Delivery") {
                var form = '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Price</p>'+
                           '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryPrice"></div>'+
                           '<div class="col-12 mt-4 p-0"><p class="c-text-2 soft-title regular-weight">Delivery Address</p>'+
                           '<input type="text" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" id="deliveryAddress"></div>';

                $("#increasePrice").html(form);
                set_main_data()
            }else{
                $("#increasePrice").html("");
            }
            totalQuant();
        });

        $("#addMaterial").click(function (e) { 
            e.preventDefault();
            addNewMate()
        });

        $("#show_again").click(function (e) { 
            e.preventDefault();
            showModMate();
        });

        $("#btnCicilan").click(function (e) { 
            e.preventDefault();
            var getQuantity = $("#tbMaterial tr").length;
            if (getQuantity > 0) {
                var payment1 = $("#paymentMethod option:selected").val();
                if (payment1 == "Cash") {
                    alert('Pilihan cash/tunai tidak menampilkan form cicilan');
                }else{
                    $("#addCicilan").modal("show");
                }
            }else{
                alert('Anda harus memilih material terlebih dahulu');
            }
        });

        $("#paymentCount").on("input", function () {
            var getCount = $(this).val();
            var payload = '';
            console.log(getCount);
            for (let i = 0; i < getCount; i++) {
                const element = getCount;

                payload += '<p id="cicilan-number-'+(i+1)+'" class="c-text-2 soft-title mt-3 regular-weight">Payment '+(i + 1)+'</p>' +
                           '<input type="date" id="cicilan'+(i + 1)+'" class="field-cicilan col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">';
                $("#countPay").html(payload);
            }
        });

        $("#submitCicilan").click(function (e) {

            var getQuantity = $("#paymentCount").val();
            var getTotal = parseInt($("#result").val());
            var total = getTotal / getQuantity;

            var tableload = '';
            var i;

            for(i = 0; i < getQuantity; i++){
                var dateEnd = $("#cicilan"+(i + 1)).val();

                tableload += '<tr>'+
                                '<td style="display: none"><input id="dateEndVal'+(1 + i)+'" type="text" value="'+dateEnd+'"></td>'+
                                '<td style="display: none"><input id="countVal'+(1 + i)+'" type="text" value="'+total+'"></td>'+
                                '<td style="display: none"><input id="infoVal'+(1 + i)+'" type="text" value="Cicilan ke-'+(1 + i)+'"></td>'+
                             '</tr>';

            }
                            
            $("#cicilanCountVal").val(getQuantity);
            $("#tbCicilan").append(tableload);
            $("#addCicilan").modal("hide");
            
        });

        $("#subMaterial").click(function (e) { 
            e.preventDefault();
            var getName = $("#brandName option:selected").text();
            var getQuant = $("#matQuant").val();
            var getTotal = parseInt($("#payment").val());
            var getPercent = parseInt($("#percentage").val());
            var getRow = $("table tbody tr").length;
            var getID = $("#brandName").val();
            var total = getTotal + (getQuant * 50000);
            var persen = getPercent / 100 * total;
            var result = total - (getPercent / 100 * total);
            var createID = getID+"-"+(getRow+1);
            var table = document.getElementById("mainMaterialTable");
            var tbodyRowCount = 1 + table.tBodies[0].rows.length;
            var createID = "comp"+"-"+(getRow+1);

            var payload = '<tr id="'+createID+'" class="t-item">'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getName+'</td>'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-quant">'+getQuant+'</td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="editItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-color-primary" >'+
                                            '<i class="bx bxs-pencil bx-xs text-white" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
                            
            $("#payment").val(total);
            $("#result").val(result);
            $("#discountForm").val(getPercent+"% - Rp. "+persen.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $("#countTotal").val("Rp. "+result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."));
            $("#materialCountVal").val(tbodyRowCount);
            $("#tbMaterial").append(payload);
        });

    });

    function addNewMate( a, b, c, d) {

        var getMate = $("#editName").val();
        var getBrand = $("#editBrand").val();
        var getQuant = $("#editQuant").val();
        var getPrice = $("#editPrice").val();
        var finPrice = getPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

        if (getQuant < 1) {
            getQuant = 1;
        }

        var getSumTot = parseInt(getQuant)*parseInt(getPrice);

        var getRow = $("table tbody tr").length;
        var getTotal = parseInt($("#payment").val());
        var getPercent = parseInt($("#percentage").val());

        var total = getTotal + getSumTot;
        var persen = getPercent / 100 * total;
        var result = total - (getPercent / 100 * total);
        var createID = "add"+"-"+(getRow+1);
        var table = document.getElementById("mainMaterialTable");
        var tbodyRowCount = 1 + table.tBodies[0].rows.length;

        var payload = '<tr id="'+createID+'" class="t-item">'+
                               '<td class="p-2 c-text-2 text-center" id="'+tbodyRowCount+'-name">'+getMate+'<input type="hidden" id="'+tbodyRowCount+'-input-brand" value="'+a+'"><input type="hidden" id="'+tbodyRowCount+'-input-id-brand" value="'+c+'"><input type="hidden" id="'+tbodyRowCount+'-input-mate" value="'+b+'"><input type="hidden" id="'+tbodyRowCount+'-input-id-mate" value="'+d+'"></td>'+
                               '<td class="p-2 c-text-2 text-center d-none" id="'+tbodyRowCount+'-brand">'+getBrand+'</td>'+
                               '<td class="p-2 c-text-2 text-center" id="'+tbodyRowCount+'-quant"><input type="text" id="temp-quant-'+tbodyRowCount+'" class="text-center search-fill col-4" placeholder="Quantity..." value="'+getQuant+'"></td>'+
                               '<td class="p-2 c-text-2 text-center" id="'+tbodyRowCount+'-price">'+getPrice+'</td>'+
                               '<td class="p-2 c-text-2 text-center d-none" id="'+tbodyRowCount+'-total">'+getSumTot+'</td>'+
                               '<td class="p-2 c-text-2 text-center ">'+
                                   '<a onclick="deleteItem(\''+createID+'\',\'temp-quant-'+createID+'\')">'+
                                       '<button class="ml-2 basic-btn c-soft-background" >'+
                                           '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                       '</button>'+
                                   '</a >'+
                               '</td>'+
                           '</tr>';
        $("#tbMaterial").append(payload);

        $("#payment").val(total);
        $("#result").val(result);

        $("#materialCountVal").val(tbodyRowCount);
        //func hitung total harga & diskon
        totalQuant();
    }

    //func hitung total harga & diskon
    function totalQuant() {
        var total = 0;
        var getDeli = $("#sellInteract option:selected").text();
        var getOngkir = 0;
        var getDis = $("#discountForm").val();
        console.log("raw percen "+getDis);
        var splitDis = getDis.split("%");
        console.log("percen: "+splitDis[0]);

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#temp-quant-"+(index+1);
            var totalId = "#"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            
            console.log(getQuant+" "+getPrice);
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += semi;
        });

        if (getDeli == "Delivery") {
            getOngkir = $("#deliveryPrice").val();
            console.log(getOngkir);
        }

        var persen = parseInt(splitDis[0])/100 * total;
        var final = total-persen;
        var final1 = final + parseInt(getOngkir);
        var gim =  "Rp "+final1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        var gim1 =  splitDis[0]+"% - Rp "+persen.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    
        console.log(final1);
        $("#countTotal").val(gim);
        $("#discountForm").val(gim1);
    }
    //func get total harga
    function getTotalPay() {
        var total = 0;
        var getDeli = $("#sellInteract option:selected").text();
        var getOngkir = 0;
        var getDis = $("#discountForm").val();
        var splitDis = getDis.split("%");

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#temp-quant-"+(index+1);
            var totalId = "#"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            
            console.log(getQuant+" "+getPrice);
            var semi = parseInt(getQuant) * parseInt(getPrice);
            total += semi;
        });

        if (getDeli == "Delivery") {
            getOngkir = $("#deliveryPrice").val();
            console.log(getOngkir);
        }

        var persen = parseInt(splitDis[0])/100 * total;
        var final = total-persen;
        var final1 = final + parseInt(getOngkir);
        
        console.log(final1);
        return final1;

    }
    //func get item di table material
    function materialBundle() {
        var material = [];

        $("#tbMaterial").find("tr").each(function (index, element) {
            // element == this
            var getId = $(element).find('td');
            var getId1 = $(element).find('td input');
            var quantId = "#temp-quant-"+(index+1);
            var priceId = (index+1)+"-price";
            var getMatName = getId.eq(0).text();
            var getQuant = $(quantId).val();
            var getMatId = "null";
            var getPrice = $("#"+priceId).text();
            
            var getBraId = "null";
            var getBraName = getId.eq(1).text();

            var item = {};
            item.detail_out_material_name= getMatName;
            item.detail_out_material_id = getMatId;
            item.detail_out_material_amount = getQuant;
            item.detail_out_material_price = getPrice;
            item.detail_out_merek_id = getBraId;
            item.detail_out_merek_name = getBraName;
            material.push(item);        
            
        });
        console.log(material);
        return material;

    }
    //func get item di form cicilan
    function cicilanBundle() {
        $(".field-cicilan").each(function (index, element) {
            // element == this
            var id = "#cicilan-number-"+(index+1);
            console.log(id);
            var getCicilan = $(element).val();
            var getInfo = $(id).text();
            var item = {};
            var split1 = getCicilan.split("-");
            var getDate = getCicilan+" 00:00:00";
            var getTotal = $("#countTotal").val();
            var split1 = getTotal.split(" ");
            var split2 = split1[1].split(".");
            var getFinal = split2[0]+split2[1];
            var getCount = $("#paymentCount").val();
            var getInstal = parseInt(getFinal)/parseInt(getCount);

            item.credit_id = "PAY-"+(parseInt(index)+1);
            item.credit_amount = getInstal;
            item.credit_additional_info = getInfo;
            item.credit_due_date = getDate;
            getCicilanBundle.push(item);
        });
        console.log(getCicilanBundle);
        return getCicilanBundle;
    }

    function editItem(id) {
        $("#editMaterial").modal("show");
        var idQuant = "#"+id+"-quant";
        var idName = "#"+id+"-name";
        var getAmount = $(idQuant).text();
        var getName = $(idName).text();
        console.log(getAmount);
        console.log(getName);
        
        $("#editQuant").val(getAmount);
        $("#editName").val(getName).change();

        $("#editMerkNew").click(function (e) { 
            e.preventDefault();
            var newQuant = $("#editQuant").val();
            var newName = $("#editName option:selected").text();

            $(idQuant).html(newQuant);
            $(idName).html(newName);
            $("#editMaterial").modal("hide");
            
        });
    }

    function deleteItem(id,val) {
        var fag = "#"+id;
        $(fag).remove();
        totalQuant();
    }

    function showQuant(matId, brandId, price) {
        var getMat = $("#"+matId).text();
        var getBra = $("#"+brandId).text();
        //var getPri = $("#"+priceId).text();
        
        $("#editName").val(getMat);
        $("#editPrice").val(price);
        $("#editBrand").val(getBra);

        $("#modQuantMaterial").modal("show");
        $("#addNewMaterial").modal("hide");
    }

    function showModMate() {
        $("#modQuantMaterial").modal("hide");
        $("#addNewMaterial").modal("show");
    }

    function removeDup() {
        $("#filter-material-out option").val(function(idx, val) {
            $(this).siblings("[value='"+ val +"']").remove();
        });
    }

</script>
