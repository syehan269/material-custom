    <div class="main-content col-10">
        <div class="d-inline-flex col-12 p-0 mb-4">
            <p class="mb-0 c-text-6 text-color regular-weight ml-4">Tambah Transaksi Masuk</p>
            <div class="dropdown ml-auto">
                <button class="border-0 text-white logo-pro" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    AM
                </button>
                <div class="mt-3 dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="<?php echo base_url() ?>index.php/welcome/login">Logout</a>
                </div>
            </div>
        </div>

        <div class="col-12 d-inline-flex p-0">
            <div class="col-6 main-padding">
                <button data-toggle="modal" data-target="#addNewMaterial" type="button" class="boldest-weight btn-outline col-12 c-border-primary primary-title c-main-background c-text-2">
                    Tambah Material
                </button>
                <div class="mt-4 custom-card p-3" style="min-height: 400px !important;">
                    <table class="col-12 p-3" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Brand </th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center d-none">Total</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="tb_material">
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="flex-column col-6 main-padding-l pr-0">
                <div class="col-12 p-0">
                    <p class="c-text-2 soft-title medium-weight">Nama Supplier</p>
                    <!-- <input class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Name..."> -->
                    <select name="name" id="name" class="dropdown-select2 col-12 c-text-2 search-fill main-padding-l main-padding-r" >
                        <option value="">Select Supplier</option>
                    </select>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Tipe Pembayaran</p>
                    <select id="payment" class="c-dropdown col-12 c-text-2" >
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="Cash">Cash</option>
                        <option class="dropdown-item" value="Credit">Credit</option>
                    </select>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Total Pembayaran</p>
                    <div class="p-0 d-inline-flex col-12">
                        <input id="amount" class="col-11 c-text-2 secondary-field main-padding-l main-padding-r" value="Rp 0" disabled>
                        <a href="#" class="my-auto">
                            <button id="btnRefresh" type="button" class="ml-2 my-auto basic-btn c-color-primary" >
                                <i class="bx bx-refresh bx-xs text-white" style="margin-top: 5px"></i>
                            </button>
                        </a >
                    </div>
                </div>
                <div class="col-12 mt-4 p-0">
                    <p class="c-text-2 soft-title medium-weight">Informasi Tambahan</p>
                    <textarea id="info" class="search-fill c-text-2 main-padding-r main-padding-l main-padding-t main-padding-b col-12" rows="4" style="min-height: 150px;"></textarea>
                </div>
                <button id="btn-add-in" class="btn-add col-12 text-white c-color-primary c-color-primary mt-4 c-text-2">Tambah Transaksi</button>
            </div>
        </div>
</div>

<div class="modal fade" id="addNewMaterial" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0">
            <div class="d-inline-flex col-12 px-0">
                <input type="text" id="search-in" class="col-6 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Cari Material...">
                <div class="ml-3 px-0 col-5">
                    <select class="c-dropdown col-4 c-text-2 " id="filter-material" style="width: 60%">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAoklEQVRIS+2TPQ6AIAyF2/vpatzUm+mmrh4QAxFTlfKqiRssLOX9fADTz4t/1qdiAAkXRN8Rtc02Oma3LNWQU0FzyTsIh4i6IMw8aiaWOWygmFzEfQ6iaV7r/t5WfUV3AdnEKh6yQb4R1dGEneMTXyZ51IX/4NFEJNKwyNDQwA+nTCziEJFMIk2s4q8MYhO/p16LdpcmRPC7ZgaKAaRXEEFEO3+WUBlzphGTAAAAAElFTkSuQmCC"/>
                        <option class="dropdown-item" value="">Select Material</option>
                    </select>
                </div>
                <div class="d-flex my-auto p-0">
                    <button class="btn-filter c-color-primary" id="filter">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAABm0lEQVRIS7VVS04CURCs2ms08QDqCcQTCCdR95roCfQGkuhe2HgN4QR6A+ECRqMrN2WKNJPnMJ8HSG/IMG+6uquq+xEbDm44P7IBJO0CuCN5vkxRWQCSOgAeAXRIZn0zL6L1sKQzVw7AHcxjAmAEYEjSv7WRC9AHsFOTxQDXJF+r3rcC+KOgaADgyBTFcxfAFYB9AB8AelUgWQABYor6JE3ZLEJ4d3daB5IN0MSzJHdnkBHJXnr2vwDcnTUwXaaqED4bQNI9ANPzA+CJ5GVaqaRbADfhrILGLIBIflGi6SEFCeFfAExIHmbPQYj5BWCrBPBOcq/UhfycDuOfDiQtHGgA+Ca5vSyAJ9RCHaeeXpKiKcmDSooSu3ky7e8iEpH932AlkSV5Op8tVHThCW2NGLi32FfNNpVkD59ElVmrOel8TNJFFrFg07CbQbzcPKGmq7KT+R0R8/EJoFveR5VzUAIxXdbD1c02Zrx3l152FrQy+cyydQRHEid2oqYYG2jldR3Ce/TNrS3smMaFYzetd+G0WqjlQNYuWgfkF6Z4txk2TbypAAAAAElFTkSuQmCC"/>
                    </button>
                </div>
            </div>
            <div class="mt-4 custom-card p-3">
                    <table id="addNewTable" class="col-12 p-2" width="100%">
                        <thead class="t-header primary-title">
                            <tr>
                                <th class="p-3 c-text-2 boldest-weight text-center">No.</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Material</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Brand</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Kuantitas</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Harga</th>
                                <th class="p-3 c-text-2 boldest-weight text-center">Utilitas</th>
                            </tr>
                        </thead>
                        <tbody id="newMaterialBody">
                            
                        </tbody>
                    </table>
                </div>
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button type="button" class="btn-modal-negative mr-3 medium-weight c-text-2" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modQuantMaterial" tabindex="-1" role="dialog" aria-labelledby="close" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <p class="primary-title c-text-3 boldest-weight modal-title" id="exampleModalLongTitle">Tambah Material</p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body c-main-background">
        <div class="col-12 p-0 mt-2">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Kuantitas Material</p>
            <input type="number" id="editQuant" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Kuantitas...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Material brand</p>
            <input type="text" id="editBrand" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Harga Material</p>
            <input type="text" id="editPrice" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
        <div class="col-12 p-0 mt-2 d-none">
            <p class="c-text-2 soft-title regular-weight" id="mateQuant">Nama Material</p>
            <input type="text" id="editName" class="col-12 c-text-2 search-fill main-padding-l main-padding-r" placeholder="Quantity...">
        </div>
      </div>
      <div class="modal-footer c-main-background border-0">
        <button id="show_again" type="button" class="btn-modal-negative c-text-2 mr-3 medium-weight" data-dismiss="modal">Back</button>
        <button id="addMaterial" type="button" class="btn-modal-positive medium-weight c-text-2">Add Material</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function () {
        var table="";
        $('.dropdown-select2').select2();
        $('#filter-material').select2();
        $("#search-in").on("input", function () {
            search(2, this.value);
        });

        $("#filter-material").on("change", function () {
            search(1, this.value);
        });

        $("#filter").click(function (e) { 
            e.preventDefault();
            $('#addNewTable').DataTable()
                .search('').columns()
                .search('').draw();
            $("#filter-material").val("");
            $("#search").val("");
        });

        set_filter();
        set_dropdown_modal();
        set_dropdown_brand();
        show_data();

        function dataTable(){
            table = $('#addNewTable').DataTable({
                "lengthChange": false,
                "pagingType": "full_numbers",
                oLanguage: {
                    oPaginate: {
                        sNext: '>',
                        sPrevious: '<',
                        sLast: '>>',
                        sFirst: '<<'
                    }
                },columnDefs: [
                    {
                        "targets": [0],
                        "orderable": false
                    },
                    {
                        "searchable": false,
                        "orderable": false,
                        "targets": 0
                    },
                    {
                        "targets": 2,
                        "visible": false
                    },
                    {
                        "targets": 3,
                        "visible": false
                    } 
                ]
            });
        }

        function search(col, getIn) {
            table
                .column(col)
                .search(getIn)
                .draw();
        }

        function set_dropdown_brand(){
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/merek",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){
                        var item = obj.data[i].merek_name; 
                        payload += '<option value="'+item+'">'+obj.data[i].merek_name+'</option>';
                        $("#brandName").html(payload);
                        $("#editName").html(payload);
                    }
                }
            });
        }

        function show_data(){

            $.ajax({
                type  : 'GET',
                url   : 'http://153.92.4.88:8080/material',
                async : true,
                dataType : 'text',
                success : function(data){
                    var html = '';
                    var i;
                    var text = data;
                    obj = JSON.parse(text);
                    for(i=0; i<obj.data.length; i++){
                        var setId = "new-mat-"+i;
                        var setMate = setId+"-mate";
                        var setBrand = setId+"-brand";
                        var setQuant = setId+"-quant";
                        var setPrice = setId+"-price";
                        html += '<tr>'+
                                    '<td class="p-3 c-text-2 text-center">'+(i + 1)+'</td>'+
                                    '<td id='+setMate+' class="p-3 c-text-2 text-center">'+obj.data[i].material_name+'</td>'+
                                    '<td id='+setBrand+' class="p-3 c-text-2 text-center">'+obj.data[i].material_merek_name+'</td>'+
                                    '<td id='+setQuant+' class="p-3 c-text-2 text-center">1</td>'+
                                    '<td id='+setPrice+' class="p-3 c-text-2 text-center">'+obj.data[i].material_price+'</td>'+
                                    '<td class="text-center">'+
                                        '<a href="#" class="" onclick="showQuant(\''+setMate+'\',\''+setBrand+'\',\''+setPrice+'\')">'+
                                            '<button class="ml-2 basic-btn c-color-primary">'+
                                                '<i class="bx bx-message-square-add text-white " style="margin-top: 5px;"></i>'+
                                            '</button>'+
                                        '</a>'+
                                    '</td>'+
                                '</tr>';                        
                    }
                    $('#newMaterialBody').html(html);
                    dataTable();
                }
            });
        }

        function set_filter() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/suppliers",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value="'+obj.data[i].supplier_name+'">'+obj.data[i].supplier_name+'</option>';
                        $("#name").html(payload);
                    }
                }
            });
        }

        function set_dropdown_modal() {
            $.ajax({
                type: "GET",
                url: "http://153.92.4.88:8080/material",
                async: true,
                dataType: "text",
                success: function (response) {
                    var payload = '';
                    var i;
                    obj = JSON.parse(response);
                    
                    for(i=0; i<obj.data.length; i++){

                        payload += '<option value='+obj.data[i].material_name+'>'+obj.data[i].material_name+'</option>';
                        $("#mat_name").html(payload);
                        $("#filter-material").html(payload);
                    }
                }
            });
        }

        function sendData() {
            var bundle = getTable();
            var getSupplier = $("#name").text();
            var getPayment = $("#payment").val();
            var getAmount = totalPayment();
            var getInfo = $("#info").val();
            var getDate = moment().format('YYYY/MM/DDThh:mm:ss');
            console.log(getDate);

            if (getInfo.length == 0) {
                getInfo = "null";                
            }

            request = $.ajax({
                url: 'http://153.92.4.88:8080/transaction-in',
                type: 'post',
                data: {
                    trans_in_payment_amount: getAmount,
                    trans_in_payment_type: getPayment,
                    trans_in_additional_info: getInfo,
                    trans_in_due_date: getDate,
                    trans_in_detail: bundle
                }
            });
            request.done(function (response) {  
                window.location.href = "<?php echo base_url() ?>index.php/c_income";
            });
            request.fail(function(response) {
              var success = response.success;
              var message = response.message;
              var data = response.data;
          });
        }

        $("#show_again").click(function (e) { 
            e.preventDefault();
            showModMate();
        });

        $("#subMaterial").click(function (e) { 
            e.preventDefault();
            var getName = $("#brandName option:selected").text();
            var getQuant = $("#matQuant").val();
            var getRow = $("table tbody tr").length;
            var getID = $("#brandName").val();
            var createID = "comp"+"-"+(getRow+1);

            var payload = '<tr class="t-item" id="'+createID+'">'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getName+'</td>'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-quant">'+getQuant+'</td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
            $("#tb_material").append(payload);
        });

        $("#btnRefresh").click(function (e) { 
            e.preventDefault();
            totalQuant();
        });

        $("#addMaterial").click(function (e) { 
            e.preventDefault();
            addNewMate()
        });

        $("#btn-add-in").click(function (e) { 
            e.preventDefault();
            if ($("#tb_material tr").length < 1) {
                alert("Insert material first !");
            }else{
                sendData();
            }
        });


    });


    //get batch data table material
    function getTable() {
        var material = [];
        var getLong = $("#tb_material tr").length;

        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var getId = $(element).find('td');
            var getId1 = $(element).find('td input');
            var quantId = "#add-"+(index+1)+"-quant";
            var totalId = "#add-"+(index+1)+"-price";
            var brandId = "#add-"+(index+1)+"-brand";
            var getMatName = getId.eq(0).text();
            var getMatId = "null";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();;
            var getBraId = "null";
            var getBraName = $(brandId).text();
            var getSupName = "PT BETON KUAT";
            var getSupId = "SUPLL-000004";

            var item = {};
            item.detail_in_material_name= getMatName;
            item.detail_in_material_id = getMatId;
            item.detail_in_material_amount = getQuant;
            item.detail_in_material_price = getPrice;
            item.detail_in_merek_id = getBraId;
            item.detail_in_merek_name = getBraName;
            item.detail_in_supplier_name = getSupName;
            item.detail_in_supplier_id = getSupId;
            material.push(item);            
            
        });
        return material;
    }

    function addNewMate() {

        var getMat = $("#editName").val();
        var getBrand = $("#editBrand").val();
        var getQuant = $("#editQuant").val();
        var getPrice = $("#editPrice").val();

        
        var getRow = $("#tb_material tr").length;
        var createID = "add"+"-"+(getRow+1);

        if (getQuant < 1) {
            getQuant = 1;
        }

        var finalPrice = parseInt(getQuant)*parseInt(getPrice);
        var finTotal = finalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        console.log("total "+finalPrice);
        var tod = "#"+createID+"-quant";

         var payload = '<tr id="'+createID+'" class="t-item">'+
                                '<td class="p-2 c-text-2 text-center" id="'+createID+'-name">'+getMat+'</td>'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-brand">'+getBrand+'</td>'+
                                '<td class="p-2 c-text-2 text-center"><input id="'+createID+'-quant" type="text" class="text-center search-fill col-4 quant-total" value="'+getQuant+'"></td>'+
                                '<td class="p-2 c-text-2 text-center " id="'+createID+'-price">'+getPrice+'</td>'+
                                '<td class="p-2 c-text-2 text-center d-none" id="'+createID+'-total">'+finTotal+'</td>'+
                                '<td class="p-2 c-text-2 text-center ">'+
                                    '<a href="#" onclick="deleteItem(\''+createID+'\')">'+
                                        '<button class="ml-2 basic-btn c-soft-background" >'+
                                            '<i class="bx bxs-trash bx-xs primary-title" style="margin-top: 5px"></i>'+
                                        '</button>'+
                                    '</a >'+
                                '</td>'+
                            '</tr>';
        $("#tb_material").append(payload);
        totalQuant();
    }

    function editItem(id) {
        $("#editMaterial").modal("show");
        var idQuant = "#"+id+"-quant";
        var idName = "#"+id+"-name";
        var getAmount = $(idQuant).text();
        var getName = $(idName).text();
        console.log(getAmount);
        console.log(getName);
        
        $("#editQuant").val(getAmount);
        $("#editName").val(getName).change();

        $("#editMerkNew").click(function (e) { 
            e.preventDefault();
            var newQuant = $("#editQuant").val();
            var newName = $("#editName option:selected").text();

            $(idQuant).html(newQuant);
            $(idName).html(newName);
            $("#editMaterial").modal("hide");
            
        });
    }

    function totalQuant() {
        var total = 0;
        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            var totalId = "#add-"+(index+1)+"-total";
            var priceId = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            var getSingle = $(priceId).text();
            var semi = parseInt(getQuant) * parseInt(getSingle);
            var finTotal = "Rp "+semi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $(totalId).val(finTotal);
            total += semi;

            console.log("quant: "+getQuant);
            console.log("sing: "+getSingle);
            console.log("total: "+getPrice);
        });

        var gim =  "Rp "+total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        $("#amount").val(gim);
    }

    function totalPayment() {
        var total = 0;
        $("#tb_material").find("tr").each(function (index, element) {
            // element == this
            var quantId = "#add-"+(index+1)+"-quant";
            var totalId = "#add-"+(index+1)+"-total";
            var priceId = "#add-"+(index+1)+"-price";
            var getQuant = $(quantId).val();
            var getPrice = $(totalId).text();
            var getSingle = $(priceId).text();
            var semi = parseInt(getQuant) * parseInt(getSingle);
            var finTotal = "Rp "+semi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            $(totalId).val(finTotal);
            total += semi;

        });

        return total;
    }

    function showQuant(matId, brandId, priceId) {
        var getMat = $("#"+matId).text();
        var getBra = $("#"+brandId).text();
        var getPri = $("#"+priceId).text();
        
        $("#editName").val(getMat);
        $("#editPrice").val(getPri);
        $("#editBrand").val(getBra);

        $("#modQuantMaterial").modal("show");
        $("#addNewMaterial").modal("hide");
    }

    function showModMate() {
        $("#modQuantMaterial").modal("hide");
        $("#addNewMaterial").modal("show");
    }

    function deleteItem(id) {
        var fag = "#"+id;
        $(fag).remove();
        totalQuant();
    }

</script>